/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.plugins.editor.source.settings;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.sal.api.user.UserManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext.GLOBAL_CONTEXT;

/**
 *
 */
public class DefaultSourceEditorConfigurationManager implements SourceEditorConfigurationManager
{
    private static final String SOURCE_EDITOR_ALLOW_ALL = "source-editor.allow-all";
    private static final String SOURCE_EDITOR_ACCESS_GROUPS = "source-editor.access-groups";
    private static final boolean SOURCE_EDITOR_ALLOW_ALL_DEFAULT = true;
    
    private final BandanaManager bandanaManager;
    private final UserManager userManager;

    public DefaultSourceEditorConfigurationManager(final BandanaManager bandanaManager, UserManager userManager)
    {
        this.bandanaManager = bandanaManager;
        this.userManager = userManager;
    }

    @Override
    public boolean getAllowAllUsers()
    {
        Boolean allowAll = (Boolean) bandanaManager.getValue(GLOBAL_CONTEXT, SOURCE_EDITOR_ALLOW_ALL);
        if(allowAll == null) 
            allowAll = SOURCE_EDITOR_ALLOW_ALL_DEFAULT;
        
        return allowAll;
    }

    @Override
    public List<String> getAccessGroups()
    {
        List<String> groups = (List<String>) bandanaManager.getValue(GLOBAL_CONTEXT, SOURCE_EDITOR_ACCESS_GROUPS);
        if(groups == null)
            groups = Collections.emptyList();

        // return copy
        return new ArrayList<String>(groups);
    }

    @Override
    public void setAllowAllUsers(final boolean allow)
    {
        bandanaManager.setValue(GLOBAL_CONTEXT, SOURCE_EDITOR_ALLOW_ALL, allow);
    }

    @Override
    public void setAccessGroups(final List<String> groups)
    {
        bandanaManager.setValue(GLOBAL_CONTEXT, SOURCE_EDITOR_ACCESS_GROUPS, groups);
    }

    @Override
    public boolean hasAccess()
    {
        String user = userManager.getRemoteUsername();
        if(user == null)
        {
            // no anonymous access
            return false;
        }

        if(getAllowAllUsers())
        {
            return true;
        }

        List<String> groups = getAccessGroups();
        for(String group : groups)
        {
            if(userManager.isUserInGroup(user, group))
                return true;
        }

        return false;
    }
}
