/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.plugins.editor.source.rest;

import com.atlassian.confluence.content.render.xhtml.*;
import com.atlassian.confluence.content.render.xhtml.editor.EditorConverter;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.editor.source.settings.SourceEditorConfigurationManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.xml.XhtmlEntityResolver;
import com.atlassian.renderer.RenderContext;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.spring.container.ContainerManager;
import org.apache.commons.lang.StringUtils;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

@Path("/convert")
public class FormatConversionResource
{
    private static final String[] STRIP_TAGS = {
        "<storage-format>",
        "<storage-format/>",
        "</storage-format>",
    };
    
    private static final String[] BLANKS = { "", "", "" };

    private static final Logger log = LoggerFactory.getLogger(FormatConversionResource.class);

    private final Renderer editRenderer;
    private final EditorConverter editConverter;
    private final XhtmlCleaner storageFormatCleaner;
    private final PageManager pageManager;
    private final SourceEditorConfigurationManager sourceEditorConfigurationManager;
    private final UserManager userManager;
    private final PermissionManager permissionManager;

    public FormatConversionResource(EditorConverter editConverter, XhtmlCleaner storageFormatCleaner, PageManager pageManager, 
                                    SourceEditorConfigurationManager sourceEditorConfigurationManager, UserManager userManager,
                                    PermissionManager permissionManager)
    {
        // Hack as there's a view and edit implementation in renderingContext.xml
        this.editRenderer = (Renderer) ContainerManager.getComponent("editRenderer");
        this.editConverter = editConverter;
        this.storageFormatCleaner = storageFormatCleaner;
        this.pageManager = pageManager;
        this.sourceEditorConfigurationManager = sourceEditorConfigurationManager;
        this.userManager = userManager;
        this.permissionManager = permissionManager;
    }

    @Path("/toStorageFormat")
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Encoded()
    public Response getStorageFormat(@FormParam("pageId") String pageId, @FormParam("blogId") String blogId,
                                     @FormParam("templateId") String templateId,
                                     @FormParam("editorFormat") String editorFormat) throws Exception
    {
        if(!sourceEditorConfigurationManager.hasAccess()) 
        {
            log.warn("User (" + userManager.getRemoteUsername() + ") does not have access to the source editor.");
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        ContentEntityObject contentEntityObject = null;
        if(templateId != null) {
            // skip - no contentEntityObject
        } else {
            contentEntityObject = getContentEntityObject(pageId, blogId);

            if(contentEntityObject == null) {
                // either page does not exist, or the user does not have access to it - treat it as missing
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        ConversionContext conversionContext = getConversionContext(contentEntityObject);

        String storageFormat = convertContentToStorageFormat(editorFormat, conversionContext);
        storageFormat = storageFormatCleaner.cleanQuietly(storageFormat, conversionContext);
        storageFormat = makePretty(storageFormat);
        return Response.ok(new ConversionResponse(storageFormat)).build();
    }

    @Path("/toEditorFormat")
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getEditorFormat(@FormParam("pageId") String pageId, @FormParam("blogId") String blogId,
                                    @FormParam("templateId") String templateId,
                                    @FormParam("storageFormat") String storageFormat) throws IOException
    {
        if(!sourceEditorConfigurationManager.hasAccess())
        {
            log.warn("User (" + userManager.getRemoteUsername() + ") does not have access to the source editor.");
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        log.error("TemplateId = " + templateId);

        ValidationError error = validate(storageFormat);
        if(error != null) 
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
        }
        else
        {
            ContentEntityObject contentEntityObject = null;
            if(templateId != null) {
                // skip - no contentEntityObject
            } else {
                contentEntityObject = getContentEntityObject(pageId, blogId);

                if(contentEntityObject == null) {
                    // either page does not exist, or the user does not have access to it - treat it as missing
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
            }

            ConversionContext conversionContext = getConversionContext(contentEntityObject);
            storageFormat = StringUtils.replaceEach(storageFormat, new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<storage-format>", "</storage-format>" }, new String[] { "", "", "" });
            String editorFormat = editRenderer.render(storageFormat, conversionContext);
            return Response.ok(new ConversionResponse(editorFormat)).build();
        }
    }

    private ContentEntityObject getContentEntityObject(String pageId, String blogId)
    {
        ContentEntityObject contentEntityObject = null;
        if(pageId != null)
        {
            contentEntityObject = getPage(pageId);
        }
        else if(blogId != null)
        {
            contentEntityObject = getBlogPost(blogId);
        }
        return contentEntityObject;
    }

    private BlogPost getBlogPost(String blogId)
    {
        BlogPost blog = null;
        try
        {
            blog = pageManager.getBlogPost(Long.parseLong(blogId));
            if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.EDIT, blog))
            {
                // No permission - prevent access
                blog = null;
            }
        }
        catch(NumberFormatException e)
        {
            // ignore
        }
        return blog;
    }

    private Page getPage(String pageId)
    {
        Page page = null;
        try 
        {
            page = pageManager.getPage(Long.parseLong(pageId));
            if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.EDIT, page))
            {
                // No permission - prevent access
                page = null;
            }
        }
        catch(NumberFormatException e)
        {
            // ignore
        }
        return page;
    }
    
    private ConversionContext getConversionContext(ContentEntityObject page)
    {
        RenderContext renderContext = page != null ? page.toPageContext() : new PageContext();
        return new DefaultConversionContext(renderContext);
    }

    private String convertContentToStorageFormat(String wysiwygContent, ConversionContext conversionContext)
            throws XhtmlParsingException, XhtmlException
    {
        return editConverter.convert(wysiwygContent, conversionContext);
    }
    
    private String makePretty(String storageFormat) throws Exception 
    {
        Document doc = getStorageDocument(storageFormat);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        // SOURCE-63 - Need to implement an xhtml version
        Properties htmlProperties = OutputPropertiesFactory.getDefaultMethodProperties("xml");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "2");

        NodeList children = doc.getDocumentElement().getChildNodes();
        for(int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            transformer.transform(new DOMSource(child), result);
        }

        return writer.toString();
    }
    
    private ValidationError validate(String storageFormat)
    {
        try
        {
            // Just validating for well-formedness at the moment.
            Document doc = getStorageDocument(storageFormat);
            // Passed validation
            return null;
        } catch (SAXParseException e) {
            return new ValidationError(e.getMessage(), e.getLineNumber(), e.getColumnNumber());
        } catch (Exception e) {
            return new ValidationError(e.getMessage());
        }
    }
    
    private Document getStorageDocument(String storageFormat) throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setExpandEntityReferences(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        XhtmlEntityResolver xmlResolver = new XhtmlEntityResolver();
        String entityDTD = xmlResolver.createDTD();
        String xmlStorageFormat =
                "<!DOCTYPE xml [ " +
                entityDTD + "]>" +
                "<storage-format>" + storageFormat + "</storage-format>";

        if(log.isDebugEnabled())
        {
            log.debug("xmlStorageFormat=\n" + xmlStorageFormat);
        }

        db.setEntityResolver(xmlResolver);

        InputSource is = new InputSource(new StringReader(xmlStorageFormat));
        Document doc = db.parse(is);
        return doc;
    }
    
    @XmlRootElement(name = "response")
    private class ValidationError
    {
        @XmlAttribute
        private final String message;

        @XmlAttribute
        private final Integer line;

        @XmlAttribute
        private final Integer column;
        

        private ValidationError(String message) {
            this(message, null, null);
        }

        private ValidationError(String message, Integer line, Integer column) {
            this.message = message;
            this.line = line;
            this.column = column;
        }

        public String getMessage() {
            return message;
        }
    }
    
    @XmlRootElement(name = "response")
    private class ConversionResponse
    {
        @XmlAttribute
        private final String content;

        private ConversionResponse(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }
    }
    

}
